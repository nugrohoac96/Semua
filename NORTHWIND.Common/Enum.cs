﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NORTHWIND.Common
{
    public enum GridFilterDataType
    {
        Text = 0,
        DateTime,
        Boolean,
        Int,
        Date
    }

    public enum PageState
    {
        New = 0,
        View = 1,
        Edit = 2
    }
}
