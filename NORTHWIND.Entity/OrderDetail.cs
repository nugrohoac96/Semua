﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PetaPoco;

namespace NORTHWIND.Entity
{
    [TableName("Order Details")]
    [PrimaryKey("OrderID,ProductID")]
    [ExplicitColumns]
    [Serializable]
    public class OrderDetail
    {
        [PetaPoco.Column]
        public int OrderID { get; set; }

        [PetaPoco.Column]
        public int ProductID { get; set; }

        [PetaPoco.Column]
        public decimal UnitPrice { get; set; }

        [PetaPoco.Column]
        public int Quantity { get; set; }

        [PetaPoco.Column]
        public Single Discount { get; set; }
    }

    [Serializable]
    public class OrderDetailView
    { 
        public int OrderID { get; set; } 
        public int ProductID { get; set; } 
        public string ProductName { get; set; } 
        public decimal UnitPrice { get; set; } 
        public int Quantity { get; set; } 
        public Single Discount { get; set; }
    }
}
