﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NORTHWIND.Entity;
using System.Security.Cryptography;

namespace NORTHWIND.Facade
{
    public class UsersFacade : BaseCRUD<Users>
    {

        public string GetUserVerifyPassword(string password)
        {
            return GetSHA1Hash(password) + GetMd5Hash(password);
        }

        static string GetMd5Hash(string input)
        {
            input = ReverseString(input);
            MD5 md5Hash = MD5.Create();

            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));


            StringBuilder sBuilder = new StringBuilder();


            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }


            return sBuilder.ToString().ToUpper();
        }

        static string GetSHA1Hash(string input)
        {
            SHA1 sha1Hash = SHA1.Create();

            byte[] data = sha1Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }


            return sBuilder.ToString().ToUpper();
        }

        public static string ReverseString(string s)
        {
            char[] arr = s.ToCharArray();
            Array.Reverse(arr);
            return new string(arr);
        }
    }
}
