﻿using NORTHWIND.Common;
using NORTHWIND.Entity;
using NORTHWIND.Facade;
using NORTHWIND.WebMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NORTHWIND.WebMVC.Controllers
{
    public class EmployeesController : Controller
    {
        // GET: Employees
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Route("Paging")]
        public ActionResult Paging(PageData data)
        {
            var facade = new EmployeesFacade();
            Dictionary<string, object> param = new Dictionary<string, object>();
            if (data.criteria != null)
                foreach (var crt in data.criteria)
                {
                    param[crt.criteria] = crt.value;
                }
            //query
            var rows = facade.GetListItemsByLikeCriteria(param, data.page, data.pageSize, data.order);
            var list = new List<EmployeesMap>();
            foreach (var item in rows)
            {
                var objEmp = new EmployeesMap();
                objEmp.Address = item.Address;
                objEmp.BirthDate = item.BirthDate;
                objEmp.City = item.City;
                objEmp.Country = item.Country;
                objEmp.EmployeeID = item.EmployeeID;
                objEmp.Extension = item.Extension;
                objEmp.FirstName = item.FirstName;
                objEmp.HireDate = item.HireDate;
                objEmp.HomePhone = item.HomePhone;
                objEmp.LastName = item.LastName;
                objEmp.Notes = item.Notes;
                objEmp.Password = item.Password;
                objEmp.Photo = item.Photo;
                var len = item.Photo.Length;
                objEmp.PhotoPath = item.PhotoPath;
                objEmp.PostalCode = item.PostalCode;
                objEmp.ReportsTo = item.ReportsTo;
                objEmp.StrPhoto = len > 78 ? Convert.ToBase64String(objEmp.Photo, 78, len - 78): "";
                objEmp.Title = item.Title;
                objEmp.TitleOfCourtesy = item.TitleOfCourtesy;
                objEmp.UserID = item.UserID;

                list.Add(objEmp);
            }

            var count = facade.GetListItemsByLikeCriteriaCount(param);
            var pageCount = Math.Ceiling(Commons.GetDouble(count) / data.pageSize);
            var apiResponse = ApiResponse.OK(new { rows = list, rowCount = count, pageCount = pageCount });
            return Json(apiResponse);
        }

        // GET: Employees/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Employees/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Employees/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Employees/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Employees/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Employees/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Employees/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
