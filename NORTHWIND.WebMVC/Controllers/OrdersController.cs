﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NORTHWIND.Common;
using NORTHWIND.Facade;
using NORTHWIND.WebMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

//D:\Project\NorthWindMVC-AdminLTE\NORTHWIND.WebMVC\Controllers\OrdersController.cs
namespace NORTHWIND.WebMVC.Controllers
{
    public class OrdersController : Controller
    {
        // GET: Orders
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Route("Paging")]        
        public ActionResult Paging(PageData data)
        {
            var facade = new OrdersFacade();
            Dictionary<string, object> param = new Dictionary<string, object>();
            if (data.criteria != null)
                foreach (var crt in data.criteria)
                {
                    param[crt.criteria] = crt.value;
                }
            var page = facade.GetPageByCriteria(param, data.page, data.pageSize, data.order);
            var rows = page.Items;
            var count = page.TotalItems;
            var pageCount = page.TotalPages;
            var apiResponse = ApiResponse.OK(new { rows = rows, rowCount = count, pageCount = pageCount });
            return CustomJsonResult.Build(apiResponse);
        }
    }
}